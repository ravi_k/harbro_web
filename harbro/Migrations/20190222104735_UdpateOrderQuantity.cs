﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Harbro.Web.Migrations
{
    public partial class UdpateOrderQuantity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Quantity",
                table: "Orders",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Quantity",
                table: "Orders",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}

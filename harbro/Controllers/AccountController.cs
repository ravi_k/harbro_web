﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Harbro.Web.Data.Repository;
using Harbro.Web.ViewModel.Account;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Harbro.Web.Data.Model;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using System;
using Harbro.Web.Controllers.Extensions;
using Harbro.Web.ViewModel.Message;
using Harbro.Web.Utility;
using System.Net.Mail;
using Harbro.Web.Data;

namespace Harbro.Web.Controllers {

    public class AccountController : BaseController {

        private UserRepository repository;
        private UserPasswordResetRepository passwordResetRepository;
        private ILogger<AccountController> logger;
        private CustomerContext customerContext;
        private DataContext context;

        public AccountController(UserRepository repository, UserPasswordResetRepository passwordResetRepository, ILogger<AccountController> logger,CustomerContext customerContext, DataContext context) {
            this.repository = repository;
            this.passwordResetRepository = passwordResetRepository;
            this.logger = logger;
            this.customerContext = customerContext;
            this.context = context;
            }

        public IActionResult Login() {
            if (User.Identity.IsAuthenticated) {
                return RedirectToAction("Index", "Home");
                }

            return View();
            }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model) {
            if (!ModelState.IsValid) {
                return View(model);
                }

            var user = await repository.VerifyLogin(model.EmailAddress, model.Password);

            if (user != null) {
                var claimsIdentity = GetClaimsIdentity(user);

                var cookies = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), new AuthenticationProperties {
                    IsPersistent = true
                    });

                await cookies;

                if (Request.Query.Keys.Contains("ReturnUrl")) {
                    return Redirect(Request.Query["ReturnUrl"].First());
                    }
                user.Accessed = DateTime.Now;
                await context.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
                }

            ModelState.AddModelError(string.Empty, "Invalid user or credentials provided for login, please try again");
            return View(model);
            }

        #region Details

        [Authorize]
        public IActionResult Details() {
            var user = repository.GetCurrentUser(User);

            var model = new AccountModel {
                Name = user.Name,
                Phone = user.Phone,
                EmailAddress = user.EmailAddress,
                ReturnUrl = Request.Headers.ContainsKey("Referer") ? Request.Headers["Referer"].ToString() : null
                };

            return View(model);
            }

        [HttpPost]
        public async Task<IActionResult> Details(AccountModel model) {
            if (!ModelState.IsValid) {
                return View(model);
                }

            var user = repository.GetCurrentUser(User);

            user.Name = model.Name;
            user.Phone = model.Phone;
            user.EmailAddress = model.EmailAddress;

            await repository.Update(user);

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            var claimsIdentity = GetClaimsIdentity(user);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), new AuthenticationProperties {
                IsPersistent = true
                });

            this.SetMessage(MessageType.Success, "Your personal details have been successfully updated.");

            if (model.ReturnUrl != null) {
                return Redirect(model.ReturnUrl);
                }

            return RedirectToAction("Index", "Home");
            }

        #endregion

        #region Reset Password       

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ResetPasswordRequest(string emailAddress) {
            string name = "";
            string url = "";

            var user = await repository.GetByEmailAddress(emailAddress);

            if (user == null) {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                } else {
                var userPasswordReset = passwordResetRepository.Create(user.UserId.Value);

                name = user.Name;
                url = this.Url.Action("ResetPassword", "Account", new { @id = userPasswordReset.UserPasswordResetId }, this.Url.ActionContext.HttpContext.Request.Scheme);
                }

            var body = new string[]{
                    string.Format("Hi {0} ,",name),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Please click on the link below to reset your forgotten password"),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("{0}",url)
                };

            var mailMessage = new MailMessage {
                From = new MailAddress("appsupport@harbro.co.uk", "Harbro"),
                Subject = "Reset Forgotten Password",
                Body = string.Join(Environment.NewLine, body)
                };

            mailMessage.To.Add(emailAddress);

            //var smtpClient = new SmtpClient {
            //    Credentials = new NetworkCredential("insiso", "1Ns1s0"),
            //    Host = "smtp.sendgrid.net",
            //    Port = 2525,
            //    };

            var smtpClient = new SmtpClient("mail.harbro.co.uk");

            smtpClient.Send(mailMessage);

            return Json(new { success = "true" });
            }

        [AllowAnonymous]
        public IActionResult ResetPassword(Guid id) {
            var model = new ResetPasswordModel();

            var userPasswordReset = passwordResetRepository.GetById(id);

            if (userPasswordReset == null) {
                return RedirectToAction("Login");
                }

            model = new ResetPasswordModel {
                Reference = id
                };

            return View(model);
            }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model) {
            if (!ModelState.IsValid) {
                return View(model);
                }

            var userPasswordReset = passwordResetRepository.GetById(model.Reference);
            passwordResetRepository.ResetUserPassword(model.Reference, model.NewPassword);

            var claimsIdentity = GetClaimsIdentity(userPasswordReset.User);


            var cookies = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), new AuthenticationProperties {
                IsPersistent = true
                });

            await cookies;

            var message = string.Format("Your password has been successfully reset");

            return RedirectToAction("Index", "Home", new { @message = message });
            }

        #endregion

        #region Change Password

        [Authorize]
        [HttpGet]
        public IActionResult ChangePassword() {
            var model = new ChangePasswordModel {
                ReturnUrl = Request.Headers.ContainsKey("Referer") ? Request.Headers["Referer"].ToString() : null
                };

            return View(model);
            }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel model) {
            if (!ModelState.IsValid) {
                return View(model);
                }

            if (model.CurrentPassword == model.NewPassword) {
                ModelState.AddModelError(string.Empty, "Your new password cannot be the same as the current password");
                return View(model);
                }

            var user = repository.GetCurrentUser(User);
            user = await repository.VerifyLogin(user.EmailAddress, model.CurrentPassword);

            if (user != null) {
                await repository.ChangePassword(user, model.NewPassword);
                } else {
                ModelState.AddModelError(string.Empty, "Incorrect username or password provided for this account, please verify and try again.");
                return View(model);
                }

            if (model.ReturnUrl != null) {
                return Redirect(model.ReturnUrl);
                }

            var message = string.Format("Your password has been successfully changed");

            return RedirectToAction("Index", "Home", new { @message = message });
            }

        #endregion

        [Authorize]
        public async Task<IActionResult> Logout() {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
            }

        private ClaimsIdentity GetClaimsIdentity(User user) {
            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim(ClaimTypes.Email, user.EmailAddress),
                new Claim(ClaimTypes.Role, user.Role)
            };

            return new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            }
        }
    }
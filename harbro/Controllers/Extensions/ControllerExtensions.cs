﻿using Harbro.Web.ViewModel.Message;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Controllers.Extensions {
    public static class ControllerExtensions {

        #region Set Message

        public static void SetMessage<T>(this T controller, MessageType type, string message) where T : Controller {
            if (!IsMessageSet(controller)) {
                controller.TempData["MessageType"] = type;
                controller.TempData["Message"] = message;
            }
        }

        public static bool IsMessageSet<T>(this T controller) where T : Controller {
            return controller.TempData["Message"] != null;
        }

        #endregion
    }
}

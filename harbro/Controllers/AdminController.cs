﻿using AutoMapper;
using Harbro.Web.Controllers.Extensions;
using Harbro.Web.Data;
using Harbro.Web.Data.Constants;
using Harbro.Web.Data.Model;
using Harbro.Web.Data.Repository;
using Harbro.Web.Data.Utility;
using Harbro.Web.ViewModel.Client;
using Harbro.Web.ViewModel.Message;
using Harbro.Web.ViewModel.User;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Harbro.Web.Controllers {

    [Authorize]
    public class AdminController : BaseController{

        private UserRepository userRepository;
        private ClientRepository clientRepository;
        private ILogger<AdminController> logger;
        private readonly IMapper mapper;
        private DataContext context;
        private CustomerContext customerContext;
        private HashUtility hashUtility = new HashUtility();

        public AdminController(UserRepository userRepository, ClientRepository clientRepository, ILogger<AdminController> logger, IMapper mapper, DataContext context, CustomerContext customerContext) {
            this.userRepository = userRepository;
            this.clientRepository = clientRepository;
            this.mapper = mapper;
            this.logger = logger;
            this.context = context;
            this.customerContext = customerContext;
        }

        public IActionResult Index() {
            return View();
        }

        #region Users

        public IActionResult Users() {
            return View();
            }

        public async Task<IActionResult> GetUsers([DataSourceRequest]DataSourceRequest request) {

            var result = new DataSourceResult();

            var serializer = new JsonSerializerSettings();

            var users = await userRepository.GetAll();

            result = users.Select(p => mapper.Map<UserModel>(p)).ToList().ToDataSourceResult(request);

            return Json(result, serializer);
            }

        public ActionResult AddUser() {
            var model = new User {
                Role = UserRoleConstant.User
                };

            return View("EditUser", model);
            }

        [HttpPost]
        public async Task<ActionResult> EditUser(User model) {

            var password = "";

            model.Password = StringUtility.RandomString(6);

            password = model.Password;

            hashUtility.GetHashAndSaltString(model.Password, out string passwordHash, out string salt);

            model.Password = passwordHash;
            model.Salt = salt;

            if (!model.UserId.HasValue) {
                await userRepository.Create(model);

                if (!string.IsNullOrWhiteSpace(model.EmailAddress)) {
                    var body = new string[]{
                    string.Format("Hi {0} ,",model.Name),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("A new account associated to your e-mail has been created. Your login password is ' {0} '",password),
                    string.Format("{0}", Environment.NewLine),
                    };

                    var mailMessage = new MailMessage {
                        From = new MailAddress("appsupport@harbro.co.uk", "Harbro"),
                        Subject = "New Account Password",
                        Body = string.Join(Environment.NewLine, body)
                        };

                    mailMessage.To.Add(model.EmailAddress);

                    //var smtpClient = new SmtpClient {
                    //    Credentials = new NetworkCredential("insiso", "1Ns1s0"),
                    //    Host = "smtp.sendgrid.net",
                    //    Port = 2525,
                    //    };

                    var smtpClient = new SmtpClient("mail.harbro.co.uk");

                    smtpClient.Send(mailMessage);
                    }
                return RedirectToAction("Users");
                } else {
                await userRepository.Update(model);

                var message = string.Format("{0} has been successfully updated", model.Name);

                return RedirectToAction("EditUser", new { @id = model.UserId, @message = message });
                }
        }

        [HttpGet]
        public async Task<ActionResult> EditUser(Guid id, string message) {

            var user = await userRepository.GetById(id);

            ViewBag.Message = message;

            return View(user);
            }

        [HttpPost]
        public async Task<IActionResult> DeleteUser([DataSourceRequest]DataSourceRequest request, User user) {

           await userRepository.Delete(user.UserId.Value);

            return Json(new[] { user }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Clients

        public IActionResult Clients() {
            return View();
        }

        public IActionResult GetClients([DataSourceRequest]DataSourceRequest request) {

            var result = new DataSourceResult();

            var serializer = new JsonSerializerSettings();

            var clients = clientRepository.GetAll();

            result = clients.Select(p => mapper.Map<ClientModel>(p)).ToList().ToDataSourceResult(request);

            return Json(result, serializer);
            }

        public IActionResult CreateClient(string accountNumber) {
            var client = new Client();

            var customer = customerContext.CUSTTABLE.Where(c => c.AccountNum == accountNumber).First();

            client = new Client {
                Name = customer.Name,
                CustomerNumber = customer.AccountNum,
                ContactName = customer.ContactPersonId,
                EmailAddress = customer.Email,
                Telephone = customer.Phone,
                BillingAddress = customer.Address,
                Created = DateTime.Now
                };

            clientRepository.Create(client);

            return Json(new { success = "true" });
            }

        [HttpGet]
        public ActionResult EditClient(Guid id, string message) {

            var model = new ClientModel();

            var client = clientRepository.GetById(id);

            model = mapper.Map<ClientModel>(client);

            ViewBag.Message = message;

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditClient(Client model) {

             await clientRepository.Update(model);

            var message = string.Format("{0} has been successfully updated", model.Name);

            return RedirectToAction("EditClient", new { @id = model.ClientId, @message = message });
            }

        [HttpPost]
        public async Task<IActionResult> DeleteClient([DataSourceRequest]DataSourceRequest request, Client client) {

            await clientRepository.Delete(client.ClientId);

            return Json(new[] { client }.ToDataSourceResult(request, ModelState));
        }

        public async Task<IActionResult> GeneratePassword(Guid id) {

            var password = "";

            var client = clientRepository.GetById(id);

            client.Password = StringUtility.RandomString(6);

            password = client.Password;

            hashUtility.GetHashAndSaltString(client.Password, out string passwordHash, out string salt);

            client.Password = passwordHash;
            client.Salt = salt;

            await clientRepository.Update(client);

            if (!string.IsNullOrWhiteSpace(client.EmailAddress)) {
                var body = new string[]{
                    string.Format("Hi {0} ,",client.Name),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("A new account associated to your e-mail has been created. Your login password is ' {0} '",password),
                    string.Format("{0}", Environment.NewLine),
                };

                var mailMessage = new MailMessage {
                    From = new MailAddress("appappsupport@harbro.co.uk", "Harbro"),
                    Subject = "New Account Password",
                    Body = string.Join(Environment.NewLine, body)
                    };

                mailMessage.To.Add(client.EmailAddress);

                //var smtpClient = new SmtpClient {
                //    Credentials = new NetworkCredential("insiso", "1Ns1s0"),
                //    Host = "smtp.sendgrid.net",
                //    Port = 2525,
                //    };

                var smtpClient = new SmtpClient("mail.harbro.co.uk");

                smtpClient.Send(mailMessage);

                }

            var message = string.Format("Password {0} is successfully generated and has been sent to the customer", password);

            return RedirectToAction("EditClient", new { @id = id, @message = message });
            }

        #endregion

    }
}

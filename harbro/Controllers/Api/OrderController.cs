﻿using AutoMapper;
using Harbro.Web.Data;
using Harbro.Web.Data.Constants;
using Harbro.Web.Data.Helpers;
using Harbro.Web.Data.Model;
using Harbro.Web.Data.Model.Api;
using Harbro.Web.Data.Model.Response;
using Harbro.Web.Data.Repository;
using Harbro.Web.ViewModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Harbro.Web.Controllers.Api {

    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class OrderController : ControllerBase {

        private CustomerContext customerContext;
        private ClientRepository clientRepository;
        private OrderRepository orderRepository;
        private readonly IMapper mapper;

        public OrderController(CustomerContext customerContext, ClientRepository clientRepository, IMapper mapper, OrderRepository orderRepository) {
            this.customerContext = customerContext;
            this.clientRepository = clientRepository;
            this.mapper = mapper;
            this.orderRepository = orderRepository;
        }

        [HttpGet("orders")]
        public IActionResult GetCustomerOrders(string accountNum) {

            var model = new List<Data.Model.Api.CustomerModel>();

            var orders = customerContext.HBR_SALESLINE_VW.Where(s => s.CustAccount == accountNum && s.SalesId.StartsWith("SO")).ToList();

            for (var i = 0; i < orders.Count; i++) {
                var customerOrder = new Data.Model.Api.CustomerModel {
                    CustomerNumber = orders[i].CustAccount,
                    ItemId = orders[i].ItemId,
                    ItemName = orders[i].ItemName,
                    Quantity = orders[i].Qty_Ordered,
                    DateOrdered = orders[i].Date_Ordered.Value,
                };
                model.Add(customerOrder);
            }

            return Ok(model);
        }

        [HttpPost("reorder")]
        public async Task<IActionResult> Reorder([FromBody] OrderModel model) {
            var client = await clientRepository.GetByCustomerNumber(model.CustomerNumber);

            var today = DateTime.Now.Date;
            var dateRequired = model.DateRequired.Date;
            var dayOfTheWeek = model.DateRequired.DayOfWeek;

            if (dayOfTheWeek == DayOfWeek.Saturday || dayOfTheWeek == DayOfWeek.Sunday) {
                return BadRequest("No deliveries at the weekend.");
            } else if (dateRequired <= today.AddHours(48)) {
                return BadRequest("Please leave atleast 48 hours for delivery.");
            } else {
                var order = new Order {
                    ClientId = client.ClientId,
                    Quantity = model.Quantity,
                    DeliveryAddress = model.DeliveryAddress,
                    DateRequired = model.DateRequired,
                    ItemName = model.ItemName,
                    ItemId = model.ItemId,
                    Units = model.Units,
                    DeliveryMethod = model.DeliveryMethod,
                    Notes = model.Notes,

                    Status = StatusConstant.Pending,
                };

                await orderRepository.Create(order);

                var body = new string[]{
                    string.Format("Hi,"),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Order Details :"),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Customer Name - {0}", client.Name),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Customer Number - {0}", model.CustomerNumber),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Quantity - {0}", model.Quantity),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Units - {0}", model.Units),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Delivery Address - {0}", model.DeliveryAddress),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Delivery Method - {0}", model.DeliveryMethod),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Date Required - {0}", model.DateRequired),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Item Name - {0}", model.ItemName),
                    string.Format("{0}", Environment.NewLine),
                     string.Format("Notes - {0}", model.Notes),
                };

                var mailMessage = new MailMessage {
                    From = new MailAddress("appsupport@harbro.co.uk", "Harbro"),
                    Subject = "New Order",
                    Body = string.Join(Environment.NewLine, body)
                };

                mailMessage.To.Add("reorder@harbro.co.uk");

                var smtpClient = new SmtpClient("mail.harbro.co.uk");

                smtpClient.Send(mailMessage);

                return Ok(ResponseSuccess.Create(order));
            }
        }

        [HttpGet("address")]
        public IActionResult GetClients(string accountNum) {

            var model = new List<CustomerAddressModel>();

            var customerName = customerContext.CUSTTABLE.Where(c => c.AccountNum == accountNum).FirstOrDefault();

            var addresses = customerContext.ADDRESS.Where(c => c.Name == customerName.Name).ToList();

            for (var i = 0; i < addresses.Count; i++) {
                var newModel = new CustomerAddressModel {
                    Name = addresses[i].Name,
                    Address = addresses[i].Address,
                };
                model.Add(newModel);
            }

            return Ok(model);
        }

        [HttpPost("contactUs")]
        public IActionResult ContactUs([FromBody] ContactUsModel model) {

            var body = new string[]{
                    string.Format("Hi,"),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Customer Name - {0}",model.CustomerName),
                    string.Format("{0}", Environment.NewLine),
                    string.Format("Customer Message - {0}",model.CustomerMessage),
                    string.Format("{0}", Environment.NewLine),
                };

            var mailMessage = new MailMessage {
                From = new MailAddress("appsupport@harbro.co.uk", "Harbro Reorder"),
                Subject = "Customer Enquiry",
                Body = string.Join(Environment.NewLine, body)
            };

            mailMessage.To.Add("reorder@harbro.co.uk");

            var smtpClient = new SmtpClient("mail.harbro.co.uk");

            smtpClient.Send(mailMessage);

            return Ok();
        }
    }
}

﻿using AutoMapper;
using Harbro.Web.Data;
using Harbro.Web.Data.Helpers;
using Harbro.Web.Data.Model.Api;
using Harbro.Web.Data.Model.Response;
using Harbro.Web.Data.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Harbro.Web.Controllers.Api {

    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase{

        private CustomerContext customerContext;
        private ClientRepository clientRepository;
        private readonly IMapper mapper;
        //private readonly AppSettings appSettings;

        public AccountController(CustomerContext customerContext, ClientRepository clientRepository, IMapper mapper/*, AppSettings appSettings*/) {
            this.customerContext = customerContext;
            this.clientRepository = clientRepository;
            this.mapper = mapper;
            //this.appSettings = appSettings;
            }

        [HttpPost("login")]
       
       public IActionResult PostLogin([FromBody] LoginModel model) {
            var user = clientRepository.Authenticate(model.CustomerNumber, model.Password);

            if (user == null)
                return BadRequest(new { message = "Customer Number or password is incorrect" });

            //var tokenHandler = new JwtSecurityTokenHandler();
            //var key = Encoding.ASCII.GetBytes("Harbro Reorder App");
            //var tokenDescriptor = new SecurityTokenDescriptor {
            //    Subject = new ClaimsIdentity(new Claim[] {
            //        new Claim(ClaimTypes.Name, user.CustomerNumber.ToString())
            //    }),
            //    Expires = DateTime.UtcNow.AddDays(365),
            //    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            //    };
            //var token = tokenHandler.CreateToken(tokenDescriptor);
            //var tokenString = tokenHandler.WriteToken(token);

            // return basic user info (without password) and token to store client side
            return Ok(user.CustomerNumber);
            }

        [HttpGet("profile")]
        public async Task<IActionResult> GetClientAddress(string accountNum) {
            var model = new CustomerInfoModel();

            var user = await clientRepository.GetByCustomerNumber(accountNum);

            if (user == null)
                return BadRequest(new { message = "Customer Number is incorrect" });

            model = new CustomerInfoModel {
                CustomerName = user.Name,
                Address = user.BillingAddress
                };

            return Ok(model);
            }
        }
    }

﻿using AutoMapper;
using Harbro.Web.Data;
using Harbro.Web.Data.Model;
using Harbro.Web.Data.Repository;
using Harbro.Web.ViewModel.Client;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Controllers {
    public class LookupController : Controller {

        private ClientRepository clientRepository;
        private readonly IMapper mapper;
        private CustomerContext customerContext;

        public LookupController(ClientRepository clientRepository, IMapper mapper, CustomerContext customerContext) {

            this.clientRepository = clientRepository;
            this.mapper = mapper;
            this.customerContext = customerContext;
            }

        public IActionResult GetClients() {
            List<Client> result = new List<Client>();

            IEnumerable<Client> clients = clientRepository.GetAll();

            result = clients.ToList();

            return Json(result);
            }

        public ActionResult GetHarbroCustomers([DataSourceRequest] DataSourceRequest request) {

            var model = new List<KeyValuePair<string, string>>();

            var serializer = new JsonSerializerSettings();

            var customers = customerContext.HBR_CUSTOMER_VW.ToList();

            var allCustomers = customers.Select(m => mapper.Map<CustomerModel>(m)).ToList();

            model = allCustomers.ToDictionary(s => s.AccountNum, s => s.Name).ToList();

            return Json(model.ToDataSourceResult(request),serializer);

            }

        public ActionResult CustomerValueMapper(string[] values) {
            var indices = new List<int>();
            Microsoft.EntityFrameworkCore.DbQuery<CustomerModel> customers = customerContext.HBR_CUSTOMER_VW;
            var customerModels = customers.Select(s => mapper.Map<CustomerModel>(s)).ToList();
            var models = customerModels.GroupBy(u => u.Name).ToDictionary(s => s.Key, s => s.Key).OrderBy(s => s.Key).ToList();

            if (values != null && values.Any()) {
                int index = 0;

                foreach (var customer in models) {
                    if (values.Contains(customer.Key)) {
                        indices.Add(index);
                        }

                    index += 1;
                    }
                }
            return Json(indices);
            }
        }
    }

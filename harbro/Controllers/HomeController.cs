﻿using Harbro.Web.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Controllers {

    [Authorize]
    public class HomeController : Controller{

        private CustomerContext customerContext;

        public HomeController(CustomerContext customerContext) {
            this.customerContext = customerContext;
            }

        public IActionResult Index(string message) {

            ViewBag.Message = message;
            return View();
        }
        public IActionResult Messages() {
            return View();
        }
        public IActionResult Help() {
            return View();
        }
        public IActionResult Settings() {
            return View();
        }
    }
}

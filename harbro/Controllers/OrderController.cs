﻿using AutoMapper;
using Harbro.Web.Controllers.Extensions;
using Harbro.Web.Data;
using Harbro.Web.Data.Constants;
using Harbro.Web.Data.Model;
using Harbro.Web.Data.Repository;
using Harbro.Web.ViewModel.Message;
using Harbro.Web.ViewModel.Order;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Controllers {

    [Authorize]
    public class OrderController : BaseController{

        private UserRepository userRepository;
        private ClientRepository clientRepository;
        private OrderRepository orderRepository;
        private ILogger<OrderController> logger;
        private readonly IMapper mapper;
        private DataContext context;
        private CustomerContext customerContext;

        public OrderController(UserRepository userRepository, ClientRepository clientRepository, OrderRepository orderRepository,
            ILogger<OrderController> logger, IMapper mapper, DataContext context, CustomerContext customerContext) {
            this.userRepository = userRepository;
            this.clientRepository = clientRepository;
            this.orderRepository = orderRepository;
            this.mapper = mapper;
            this.logger = logger;
            this.context = context;
            this.customerContext = customerContext;
        }

        public IActionResult Index() {
            return View();
        }

        public async Task<IActionResult> GetOrders([DataSourceRequest]DataSourceRequest request) {

            var result = new DataSourceResult();

            var serializer = new JsonSerializerSettings();

            var orders = await orderRepository.GetAll();

            result = orders.Select(p => mapper.Map<OrderModel>(p)).ToList().ToDataSourceResult(request);

            return Json(result, serializer);
            }

        public async Task<IActionResult> ViewOrder(Guid id, string message) {
            var model = await orderRepository.GetById(id);

            var client =  clientRepository.GetById(model.ClientId);

            model.Client = client;

            ViewBag.Message = message;

            return View(model);
        }


        public async Task<IActionResult> Edit(Guid id) {
            var model = await orderRepository.GetById(id);
            var client = clientRepository.GetById(model.ClientId);
            model.Client = client;
            

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Order model) {
          
           await orderRepository.Update(model);

            var message = string.Format("{0} has been successfully updated", model.ItemName);

            return RedirectToAction("ViewOrder", new { @id = model.OrderId , @message = message });
        }

        
        public async Task<IActionResult> Cancel(Guid id) {
            var model = await orderRepository.GetById(id);

            model.DateCancelled = DateTime.Now;
            model.Status = StatusConstant.Cancelled;

           await orderRepository.Update(model);

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Deliver(Guid id) {
            var model = await orderRepository.GetById(id);

            model.DateDelivered = DateTime.Now;
            model.Status = StatusConstant.Delivered;

            await orderRepository.Update(model);

            var message = string.Format("{0} has been successfully marked as delivered", model.ItemName);

            return RedirectToAction("Index", "Home", new { @message = message });
            }

        public async Task<IActionResult> RepeatOrder(Guid id) {
            var model = await orderRepository.GetById(id);
            ViewBag.Id = id;
            return View(model);
            }

        [HttpPost]
        public async Task<IActionResult> Repeat(Order model, Guid id) {
            var order = await orderRepository.GetById(id);
            model = new Order {
                ClientId = order.ClientId,
                DeliveryAddress = model.DeliveryAddress,
                DateRequired = model.DateRequired,
                Quantity = model.Quantity,
                DeliveryMethod = model.DeliveryMethod,
                ItemId = model.ItemId,
                ItemName = model.ItemName,
                Status = model.Status,
                Units = model.Units,
                Notes = model.Notes,
            };

           await orderRepository.Create(model);

            return RedirectToAction("Index", "Home");
        }
    }
}

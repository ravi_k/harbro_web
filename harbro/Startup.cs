﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Harbro.Web.Data;
using Harbro.Web.Data.Helpers;
using Harbro.Web.Data.Repository;
using Harbro.Web.Data.Utility;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace harbro {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
            }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.Configure<CookiePolicyOptions>(options => {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddResponseCompression();

            services.AddDbContext<DataContext>(cfg => {
                cfg.UseSqlServer(Configuration.GetConnectionString("ConnectionString"));
            });

            services.AddDbContext<CustomerContext>(cfg => {
                cfg.UseSqlServer(Configuration.GetConnectionString("HarbroConnectionString"));
            });

            services.AddTransient<DataInitializer>();
            services.AddTransient<HashUtility>();

            services.AddScoped<UserRepository>();
            services.AddScoped<UserPasswordResetRepository>();
            services.AddScoped<ClientRepository>();
            services.AddScoped<OrderRepository>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddKendo();

            services.AddRouting();

            services.AddAutoMapper();

            services.AddResponseCompression();

            services.Configure<BrotliCompressionProviderOptions>(options => {
                options.Level = CompressionLevel.Fastest;
            });

            services.Configure<GzipCompressionProviderOptions>(options => {
                options.Level = CompressionLevel.Fastest;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme);

            //var appSettingsSection = Configuration.GetSection("AppSettings");
            //services.Configure<AppSettings>(appSettingsSection);

            //var appSettings = appSettingsSection.Get<AppSettings>();
            //var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            //services.AddAuthentication(x => {
            //    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //.AddJwtBearer(x => {
            //    x.Events = new JwtBearerEvents {
            //        OnTokenValidated = context => {
            //            var userService = context.HttpContext.RequestServices.GetRequiredService<IUserServiceAsync>();
            //            var clientId = Guid.Parse(context.Principal.Identity.Name);
            //            var user = userService.GetById(clientId);
            //            if (user == null) {
            //                // return unauthorized if user no longer exists
            //                context.Fail("Unauthorized");
            //                }
            //            return Task.CompletedTask;
            //        }
            //        };
            //    x.RequireHttpsMetadata = false;
            //    x.SaveToken = true;
            //    x.TokenValidationParameters = new TokenValidationParameters {
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(key),
            //        ValidateIssuer = false,
            //        ValidateAudience = false
            //        };
            //});

            // configure DI for application services
            services.AddScoped<IUserServiceAsync, ClientRepository>();

            }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                } else {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                }

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseResponseCompression();
            app.UseMvc();

            app.UseMvcWithDefaultRoute();

            //app.UseKendo(env);

            using (var scope = app.ApplicationServices.CreateScope()) {
                var context = scope.ServiceProvider.GetRequiredService<DataContext>();
                context.Database.Migrate();
                var dataInitializer = scope.ServiceProvider.GetService<DataInitializer>();
                dataInitializer.Initialize();
                }
            }
        }
    }

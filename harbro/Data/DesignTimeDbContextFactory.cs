﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data {
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext> {
        public DataContext CreateDbContext(string[] args) {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<DataContext>();
            var connectionString = configuration.GetConnectionString("ConnectionString");

            builder.UseSqlServer(connectionString);
            return new DataContext(builder.Options);
            }

        public CustomerContext CreateCustomerDbContext(string[] args) {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            DbContextOptionsBuilder<CustomerContext> builder = new DbContextOptionsBuilder<CustomerContext>();
            string accessDimensionConnectionString = configuration.GetConnectionString("HarbroConnectionString");

            builder.UseSqlServer(accessDimensionConnectionString);

            return new CustomerContext(builder.Options);
            }
        }
    }
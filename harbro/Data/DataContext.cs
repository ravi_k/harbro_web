﻿using Harbro.Web.Data.Model;
using Harbro.Web.ViewModel.Client;
using Microsoft.EntityFrameworkCore;

namespace Harbro.Web.Data {
    public class DataContext : DbContext {

        public DataContext(DbContextOptions<DataContext> options) : base(options) {
            }

        public DbSet<User> Users { get; set; }

        public DbSet<UserPasswordReset> UserPasswordResets { get; set; }

        public DbSet<ClientPasswordReset> ClientPasswordResets { get; set; }

        public DbSet<Client> Clients { get; set; }

        public DbSet<Order> Orders { get; set; }

        }

    public class CustomerContext : DbContext {
        public CustomerContext(DbContextOptions<CustomerContext> options) : base(options) {
            }
        public DbQuery<CustomerModel> HBR_CUSTOMER_VW { get; set; }

        public DbQuery<CustomerOrderModel> HBR_SALESLINE_VW { get; set; }

        public DbSet<CustomerAddressModel> ADDRESS { get; set; }

        public DbSet<CustomerDetailModel> CUSTTABLE { get; set; }
        }
    }

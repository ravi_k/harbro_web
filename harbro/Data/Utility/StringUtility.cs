﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Utility {
    public static class StringUtility {
        public static Random random = new Random((int)DateTime.Now.Ticks);

        public static string RandomString(int size) {
            var builder = new StringBuilder();
            char character;

            for (int i = 0; i < size; i++) {
                character = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(character);
                }

            return builder.ToString();
            }

        public static int RandomNumber(int min, int max) {
            Random rnum = new Random();
            return rnum.Next(min, max);
            }
        }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model.Response {
    public class Error {
        public string InternalMessage { get; set; }
        public string DisplayMessage { get; set; }
        }
    }

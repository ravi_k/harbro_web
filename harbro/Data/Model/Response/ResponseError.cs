﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model.Response {
    public class ResponseError : Response {
        public Error Error { get; set; }
        public static ResponseError Create(string displayMessage, string internalMessage = null) {
            ResponseError response = new ResponseError();
            response.Success = false;
            response.Error = new Error() {
                InternalMessage = internalMessage,
                DisplayMessage = displayMessage
                };
            return response;
            }
        }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model.Response {
    public class ResponseSuccess : Response {
        public object Data { get; set; }
        public object Message { get; set; }
        public static ResponseSuccess Create(object data, string message) {
            ResponseSuccess response = new ResponseSuccess();
            response.Success = true;
            response.Data = data;
            response.Message = message;
            return response;
            }

        public static ResponseSuccess Create(object data) {
            return Create(data, null);
            }

        public static ResponseSuccess Create(string message) {
            return Create(null, message);
            }
        }
    }

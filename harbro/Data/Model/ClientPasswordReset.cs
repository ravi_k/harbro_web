﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model {
    public class ClientPasswordReset {

        [Key]
        public Guid ClientPasswordResetId { get; set; }

        public virtual Client Client { get; set; }

        [Required]
        public Guid ClientId { get; set; }

        [Required]
        public DateTime Created { get; set; }
    }
}

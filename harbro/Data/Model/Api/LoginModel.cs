﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model.Api {
    public class LoginModel {
        public string CustomerNumber { get; set; }

        public string Password { get; set; }
        }
    }

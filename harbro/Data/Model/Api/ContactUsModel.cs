﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model.Api {
    public class ContactUsModel {
        public string CustomerName { get; set; }

        public string CustomerMessage { get; set; }

        }
    }

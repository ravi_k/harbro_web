﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model.Api {
    public class OrderModel {

        [JsonProperty("itemId")]
        public string ItemId { get; set; }

        [JsonProperty("customerNumber")]
        public string CustomerNumber { get; set; }

        [JsonProperty("itemName")]
        public string ItemName { get; set; }

        public DateTime DateRequired { get; set; }

        [JsonProperty("quantity")]
        public double Quantity { get; set; }

        public string DeliveryAddress { get; set; }

        public string Units { get; set; }

        public string Notes { get; set; }

        public string DeliveryMethod { get; set; }

    }
}

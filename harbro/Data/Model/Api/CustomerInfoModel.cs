﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model.Api {
    public class CustomerInfoModel {

        public string CustomerName { get; set; }

        public string Address { get; set; }

        public string[] BillingAddress {
            get {
               return Address.Split(new[] { "\r\n", "\r", "\n" }.ToArray(), StringSplitOptions.None);
                }
            }
        }
    }

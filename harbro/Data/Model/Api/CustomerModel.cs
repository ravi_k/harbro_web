﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model.Api {
    public class CustomerModel {

        public string CustomerNumber { get; set; }

        public string ItemId { get; set; }

        public string ItemName { get; set; }

        public Decimal Quantity { get; set; }

        public DateTime DateOrdered { get; set; }
        }
    }

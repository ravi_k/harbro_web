﻿using Harbro.Web.Data.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model {    
    public class User {

        [Key]
        public Guid? UserId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    
        [Required]
        [StringLength(255)]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(255)]
        public string Phone { get; set; }

        [Required]
        [StringLength(3)]
        public string Role { get; set; }

        [NotMapped]
        public string RoleDescription {
            get {
                return UserRoleConstant.GetDescription(this.Role);
            }
        }

        [Required]
        [StringLength(255)]
        public string Password { get; set; }

        [Required]
        [StringLength(255)]
        public string Salt { get; set; }

        public DateTime? Accessed { get; set; }

        [Required]
        public DateTime Created { get; set; }

        public ICollection<UserPasswordReset> UserPasswordResets { get; set; }
    }
}

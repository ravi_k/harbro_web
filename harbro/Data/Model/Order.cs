﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Harbro.Web.Data.Constants;

namespace Harbro.Web.Data.Model {
    public class Order {

        [Key]
        public Guid OrderId { get; set; }

        [Required]
        public Guid ClientId { get; set; }

        public string ItemId { get; set; }

        public Client Client { get; set; }

        public string ItemName { get; set; }

        [Required]
        public double Quantity { get; set; }

        [NotMapped]
        public string QuantityString {
            get {
                return this.Quantity.ToString();
                }
            }

        //[Required]
        //public int Amount { get; set; }

        public string Units { get; set; }

        public string Notes { get; set; }

        public string DeliveryMethod { get; set; }

        [Required]
        [StringLength(3)]
        public string Status { get; set; }

        [NotMapped]
        public string StatusDescription {
            get {
                return StatusConstant.GetDescription(this.Status);
                }
            }

        public string StatusTextColour {
            get {
                if (this.Status == StatusConstant.Delivered) {
                    return "#008000";
                    } else if (this.Status == StatusConstant.Pending) {
                    return "#FFA500";
                    } else if (this.Status == StatusConstant.Cancelled) {
                    return "#CC0000";
                    }
                return "#000";
                }
            }

        public bool IsDelivered {
            get {
                return this.Status == StatusConstant.Delivered;
                }
            }

        public bool IsPending {
            get {
                return this.Status == StatusConstant.Pending;
                }
            }

        public bool IsCancelled {
            get {
                return this.Status == StatusConstant.Cancelled;
                }
            }

        [Required]
        public DateTime DateOrdered { get; set; }

        public string DateString {
            get {
                return this.DateOrdered.ToString("dd-MMM-yyyy");
                }
            }

        public DateTime? DateDelivered { get; set; }

        public DateTime? DateCancelled { get; set; }

        public DateTime? DateRequired { get; set; }

        [Required]
        [StringLength(255)]
        public string DeliveryAddress { get; set; }

        public List<string> Delivery {
            get {
                var strings = new List<string>();
                return this.DeliveryAddress.Split(',').ToList();

                }
            }
        }
    }

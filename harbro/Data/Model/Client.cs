﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model {
    public class Client {

        [Key]
        public Guid ClientId { get; set; }

        public string CustomerNumber { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string ContactName { get; set; }

        [StringLength(255)]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [StringLength(255)]
        public string Telephone { get; set; }

        [Required]
        public string BillingAddress { get; set; }

        
        public List<string> Billing {
            get {
                var strings = new List<string>();
                return this.BillingAddress.Split(',').ToList();
            }
        }

        [StringLength(255)]
        public string Password { get; set; }

        [StringLength(255)]
        public string Salt { get; set; }

        public DateTime? Accessed { get; set; }

        [Required]
        public DateTime Created { get; set; }

        public ICollection<ClientPasswordReset> ClientPasswordResets { get; set; }
        }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Model {
    //[Table("UserPasswordReset")]
    public class UserPasswordReset {

        [Key]
        public Guid UserPasswordResetId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public DateTime Created { get; set; }
    }
}

﻿using Harbro.Web.Data.Model;
using Harbro.Web.Data.Utility;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Repository {
    public class UserRepository : IRepositoryAsync<User> {

        private DataContext context;
        private ILogger<UserRepository> logger;
        private HashUtility hashUtility;

        public UserRepository(DataContext context, ILogger<UserRepository> logger, HashUtility hashUtility) {
            this.context = context;
            this.logger = logger;
            this.hashUtility = hashUtility;
            }

        #region Get/Create/Update/Delete

        public async Task<IEnumerable<User>> GetAll() {
            try {
                return await context.Users.OrderBy(u => u.Name).AsNoTracking().ToListAsync();
                } catch (Exception ex) {
                logger.LogError($"Failed to retrieve all users: {ex}");
                return null;
                }
            }

        public User GetCurrentUser(ClaimsPrincipal principal) {
            try {
                var emailClaim = principal.Claims.First(c => c.Type == ClaimTypes.Email);
                var user = context.Users.FirstOrDefault(u => u.EmailAddress == emailClaim.Value);
                user.Accessed = DateTime.UtcNow;
                context.SaveChanges();
                return user;
                } catch (Exception ex) {
                logger.LogError($"Failed to retrieve current user: {ex}");
                return null;
                }
            }

        public IQueryable<User> Query() {
            try {
                return context.Users.AsQueryable();
                } catch (Exception ex) {
                logger.LogError($"Failed to query users: {ex}");
                return null;
                }
            }

        public async Task<User> GetById(Guid id) {
            try {
                return await context.Users.SingleOrDefaultAsync(u => u.UserId == id);
                } catch (Exception ex) {
                logger.LogError($"Failed to retrieve User by UserId - {id}: {ex}");
                return null;
                }
            }

        public async Task<User> GetByEmailAddress(string emailAddress) {
            try {
                return await context.Users.SingleOrDefaultAsync(u => u.EmailAddress == emailAddress);
                } catch (Exception ex) {
                logger.LogError($"Failed to retrieve User by Email Address - {emailAddress}: {ex}");
                return null;
                }
            }

        public async Task<User> Create(User model) {
            try {
                model.UserId = Guid.NewGuid();

                context.Users.Add(model);

                await context.SaveChangesAsync();

                return model;
                } catch (Exception ex) {
                logger.LogError($"Failed to create User: {ex}");
                return null;
                }
            }

        public async Task Update(User model) {
            try {
                context.Update(model);
                await context.SaveChangesAsync();
                } catch (Exception ex) {
                logger.LogError($"Failed to update User: {ex}");
                }
            }

        public async Task Delete(Guid id) {
            try {
                var targetUser = await GetById(id);
                context.Users.Remove(targetUser);
                await context.SaveChangesAsync();
                } catch (Exception ex) {
                logger.LogError($"Failed to delete User by UserId - {id}: {ex}");
                }
            }

        #endregion

        #region Login

        public async Task<User> VerifyLogin(string emailAddress, string password) {
            var user = await GetByEmailAddress(emailAddress);

            if (user != null) {
                if (hashUtility.VerifyHashString(password, user.Password, user.Salt)) {
                    return user;
                }
            }
            return null;
            }

        #endregion

        #region Change Password 

        public async Task ChangePassword(User user, string password) {
            hashUtility.GetHashAndSaltString(password, out string passwordHash, out string salt);

            user.Password = passwordHash;
            user.Salt = salt;

            await context.SaveChangesAsync();
            }

        #endregion

        }
    }
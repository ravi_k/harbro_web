﻿using Harbro.Web.Data.Model;
using Harbro.Web.Data.Utility;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Harbro.Web.Data.Constants;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Repository {
    public class OrderRepository : IRepositoryAsync<Order> {

        private DataContext context;
        private ILogger<OrderRepository> logger;
        private HashUtility hashUtility;

        public OrderRepository(DataContext context, ILogger<OrderRepository> logger, HashUtility hashUtility) {
            this.context = context;
            this.logger = logger;
            this.hashUtility = hashUtility;
        }

        #region Get/Create/Update/Delete

        public async Task<IEnumerable<Order>> GetAll() {
            try {
                return await context.Orders.Include(C => C.Client).AsNoTracking().OrderByDescending(u => u.DateOrdered).ToListAsync();
            } catch (Exception ex) {
                logger.LogError($"Failed to retrieve all orders: {ex}");
                return null;
            }
        }

        public IQueryable<Order> Query() {
            try {
                return context.Orders.AsQueryable();
            } catch (Exception ex) {
                logger.LogError($"Failed to query orders: {ex}");
                return null;
            }
        }

        public IQueryable<Order> ByClient(Guid clientId) {
            try {
                return Query().Where(r => r.ClientId == clientId).OrderBy(r => r.DateOrdered);
            } catch (Exception ex) {
                logger.LogError($"Failed to query Clients: {ex}");
                return null;
            }
        }

        public async Task<Order> GetById(Guid id) {
            try {
                return await context.Orders.AsNoTracking().SingleOrDefaultAsync(u => u.OrderId == id);
            } catch (Exception ex) {
                logger.LogError($"Failed to retrieve Order by OrderId - {id}: {ex}");
                return null;
            }
        }

        public async Task<Order> Create(Order model) {
            try {
                model.OrderId = Guid.NewGuid();
                model.DateOrdered = DateTime.Now;
                model.Status = StatusConstant.Pending;
                context.Orders.Add(model);
                await context.SaveChangesAsync();
                return model;
            } catch (Exception ex) {
                logger.LogError($"Failed to create Order: {ex}");
                return null;
            }
        }

        public async Task Update(Order model) {
            try {
                context.Update(model);
               await context.SaveChangesAsync();
            } catch (Exception ex) {
                logger.LogError($"Failed to update Order: {ex}");
            }
        }

        public async Task Delete(Guid id) {
            try {
                var targetOrder = await GetById(id);
                context.Orders.Remove(targetOrder);
               await context.SaveChangesAsync();
            } catch (Exception ex) {
                logger.LogError($"Failed to delete Order by OrderId - {id}: {ex}");
            }
        }
    }
}
    #endregion


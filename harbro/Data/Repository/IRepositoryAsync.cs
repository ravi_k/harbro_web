﻿using Harbro.Web.Data.Constants;
using Harbro.Web.Data.Model;
using Harbro.Web.Data.Utility;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Repository
{
    public interface IRepositoryAsync<T>
    {

        Task<IEnumerable<T>> GetAll();
        IQueryable<T> Query();
        Task<T> GetById(Guid id);
        Task<T> Create(T model);
        Task Update(T model);
        Task Delete(Guid id);
    }
}

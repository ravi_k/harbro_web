﻿using Harbro.Web.Data.Model;
using Harbro.Web.Data.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Harbro.Web.Data.Repository {
    public class UserPasswordResetRepository : IRepository<UserPasswordReset> {

        private DataContext context;
        private HashUtility hashUtility;
        private ILogger<UserPasswordResetRepository> logger;


        public UserPasswordResetRepository(DataContext context, ILogger<UserPasswordResetRepository> logger, HashUtility hashUtility) {
            this.context = context;
            this.logger = logger;
            this.hashUtility = hashUtility;
        }

        public void ResetUserPassword(Guid id, string password) {
            string passwordHash;
            string salt;

            hashUtility.GetHashAndSaltString(password, out passwordHash, out salt);

            var userPasswordReset = GetById(id);

            userPasswordReset.User.Password = passwordHash;
            userPasswordReset.User.Salt = salt;

            Delete(userPasswordReset);

            context.SaveChanges();
        }

        public UserPasswordReset Create(Guid userId) {
            try {
                var model = new UserPasswordReset {
                    UserPasswordResetId = Guid.NewGuid(),
                    UserId = userId,
                    Created = DateTime.UtcNow
                };

                context.UserPasswordResets.Add(model);
                context.SaveChanges();

                return model;
            } catch (Exception ex) {
                logger.LogError($"Failed to create User Password Reset: {ex}");
                return null;
            }
        }

        public void Delete(UserPasswordReset userPasswordReset) {
            try {
                var targetUserPasswordReset = GetAll().FirstOrDefault(p => p.UserPasswordResetId == userPasswordReset.UserPasswordResetId);
                context.UserPasswordResets.Remove(targetUserPasswordReset);
                context.SaveChanges();
            } catch (Exception ex) {
                logger.LogError($"Failed to delete User Password Reset by UserPasswordResetId - {userPasswordReset.UserPasswordResetId}: {ex}");
            }
        }

        public IEnumerable<UserPasswordReset> GetAll() {
            throw new NotImplementedException();
        }

        public UserPasswordReset GetById(Guid id) {
            return context.UserPasswordResets.Include(u => u.User).SingleOrDefault(u => u.UserPasswordResetId == id);
        }

        public IQueryable<UserPasswordReset> Query() {
            try {
                return context.UserPasswordResets.AsQueryable();
            } catch (Exception ex) {
                logger.LogError($"Failed to query users: {ex}");
                return null;
            }
        }

        public UserPasswordReset Create(UserPasswordReset model) {
            throw new NotImplementedException();
        }

        public void Update(UserPasswordReset model) {
            throw new NotImplementedException();
        }
    }
}

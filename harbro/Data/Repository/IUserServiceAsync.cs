﻿using Harbro.Web.Data.Model;
using Harbro.Web.Data.Utility;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Repository {
    public interface IUserServiceAsync {
        Client Authenticate(string customerNumber, string password);
        IEnumerable<Client> GetAll();
        IQueryable<Client> Query();
        Client GetById(Guid id);
        Client Create(Client model);
        Task Update(Client model);
        Task Delete(Guid id);
        }
    }

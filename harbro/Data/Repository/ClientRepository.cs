﻿using Harbro.Web.Data.Model;
using Harbro.Web.Data.Utility;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Repository {
    public class ClientRepository : IUserServiceAsync {

        private DataContext context;
        private ILogger<ClientRepository> logger;
        private HashUtility hashUtility;

        public ClientRepository(DataContext context, ILogger<ClientRepository> logger, HashUtility hashUtility) {
            this.context = context;
            this.logger = logger;
            this.hashUtility = hashUtility;
            }

        #region Get/Create/Update/Delete/Authenticate

        public Client Authenticate(string customerNumber, string password) {
            if (string.IsNullOrEmpty(customerNumber) || string.IsNullOrEmpty(password))
                return null;

            var user = context.Clients.SingleOrDefault(x => x.CustomerNumber == customerNumber);

            if (user == null)
                return null;

            if (!hashUtility.VerifyHashString(password, user.Password, user.Salt))
                return null;

            return user;
            }

        public IEnumerable<Client> GetAll() {
            try {
                return context.Clients.OrderBy(u => u.Name).AsNoTracking().ToList();
                } catch (Exception ex) {
                logger.LogError($"Failed to retrieve all clients: {ex}");
                return null;
                }
            }

        public IQueryable<Client> Query() {
            try {
                return context.Clients.AsQueryable();
                } catch (Exception ex) {
                logger.LogError($"Failed to query clients: {ex}");
                return null;
                }
            }

        public async Task<Client> GetByEmailAddress(string emailAddress) {
            try {
                return await context.Clients.SingleOrDefaultAsync(u => u.EmailAddress == emailAddress);
                } catch (Exception ex) {
                logger.LogError($"Failed to retrieve Client by Email Address - {emailAddress}: {ex}");
                return null;
                }
            }

        public async Task<Client> GetByCustomerNumber(string customerNumber) {
            try {
                return await context.Clients.SingleOrDefaultAsync(u => u.CustomerNumber == customerNumber);
                } catch (Exception ex) {
                logger.LogError($"Failed to retrieve Client by Customer Number - {customerNumber}: {ex}");
                return null;
                }
            }

        public async Task<Client> VerifyLogin(string emailAddress, string password) {
            var user = await GetByEmailAddress(emailAddress);

            if (user != null) {
                if (hashUtility.VerifyHashString(password, user.Password, user.Salt)) {
                    return user;
                    }
                }

            return null;
            }

        public async Task<Client> VerifyCustomerLogin(string customerNumber, string password) {
            var user = await GetByCustomerNumber(customerNumber);

            if (user != null) {
                if (hashUtility.VerifyHashString(password, user.Password, user.Salt)) {
                    return user;
                    }
                }

            return null;
            }

        public Client GetById(Guid id) {
            try {
                return context.Clients.SingleOrDefault(u => u.ClientId == id);
                } catch (Exception ex) {
                logger.LogError($"Failed to retrieve Client by ClientId - {id}: {ex}");
                return null;
                }
            }

        public Client Create(Client model) {
            try {
                model.ClientId = Guid.NewGuid();
                context.Clients.Add(model);
                context.SaveChanges();
                return model;
                } catch (Exception ex) {
                logger.LogError($"Failed to create Client: {ex}");
                return null;
                }
            }

        public async Task Update(Client model) {
            try {
                context.Update(model);
                await context.SaveChangesAsync();
                } catch (Exception ex) {
                logger.LogError($"Failed to update Client: {ex}");
                }
            }

        public async Task Delete(Guid id) {
            try {
                var client = GetById(id);
                context.Clients.Remove(client);
                context.SaveChanges();
                } catch (Exception ex) {
                logger.LogError($"Failed to delete Client by ClientId - {id}: {ex}");
                }
            }
        }
    }
#endregion
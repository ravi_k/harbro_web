﻿using Harbro.Web.Data.Model;
using Harbro.Web.Data.Utility;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Harbro.Web.Data.Repository {
    public interface IRepository<T> {

        IEnumerable<T> GetAll();
        IQueryable<T> Query();
        T GetById(Guid id);
        T Create(T model);
        void Update(T model);
        void Delete(T model);
    }
}

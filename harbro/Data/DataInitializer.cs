﻿using Harbro.Web.Data.Constants;
using Harbro.Web.Data.Model;
using Harbro.Web.Data.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data {
    public class DataInitializer {

        private DataContext context;
        private ILogger<DataInitializer> logger;

        private HashUtility hashUtility = new HashUtility();

        public DataInitializer(DataContext context, ILogger<DataInitializer> logger) {
            this.context = context;
            this.logger = logger;
        }

        public void Initialize() {
            context.Database.EnsureCreated();

            if (!context.Users.Any()) {
                logger.LogInformation("No data found, creating sample data");

                var arrash = CreateUser(context, UserRoleConstant.Administrator, "Arrash Nekonam", "arrash.nekonam@insiso.co.uk","password");

                var ravi = CreateUser(context, UserRoleConstant.Administrator, "Ravi Kumar", "ravi.kumar@insiso.co.uk", "password");

                context.SaveChanges();
            }

            if (!context.Clients.Any()) {
                logger.LogInformation("No data found, creating sample data");

                var hillTopFarm = CreateClient(context,"Hilltop Farm", "Client 1","01224 123456", "hilltop@hilltopfarm.com","Hilltop Farm, Turriff, Aberdeenshire, AB53 4PA", "password");

                var sunnySideFarm = CreateClient(context, "Sunnyside Farm", "Client 1", "01224 734415", "sunnyside@sunnysidefarm.com", "Sunnyside House, Maryculter, Aberdeen AB12 5GT", "password");

                CreatedOrder(context, hillTopFarm, 0.18, StatusConstant.Delivered, DateTime.Now.AddDays(-19), DateTime.Now, null, "Hilltop Farm, Turriff, Aberdeenshire, AB53 4PA", "Delivery");
                CreatedOrder(context, hillTopFarm, 0.25,  StatusConstant.Pending, DateTime.Now.AddDays(-3),null, null, "Hilltop Farm, Turriff, Aberdeenshire, AB53 4PA", "Collection");
                CreatedOrder(context, hillTopFarm, 0.025, StatusConstant.Delivered, DateTime.Now.AddDays(-7), DateTime.Now, null, "Hilltop Farm, Turriff, Aberdeenshire, AB53 4PA", "Delivery");
                CreatedOrder(context, hillTopFarm, 0.19, StatusConstant.Pending, DateTime.Now.AddDays(-4),null, null, "Hilltop Farm, Turriff, Aberdeenshire, AB53 4PA", "Collection");
                CreatedOrder(context, hillTopFarm, 0.19, StatusConstant.Delivered, DateTime.Now.AddDays(-15), DateTime.Now, null, "Hilltop Farm, Turriff, Aberdeenshire, AB53 4PA", "Collection");
                CreatedOrder(context, hillTopFarm, 0.025, StatusConstant.Cancelled, DateTime.Now.AddDays(-6), null, DateTime.Now, "Hilltop Farm, Turriff, Aberdeenshire, AB53 4PA", "Collection");
                CreatedOrder(context, hillTopFarm, 0.025, StatusConstant.Cancelled, DateTime.Now.AddDays(-5), null, DateTime.Now, "Hilltop Farm, Turriff, Aberdeenshire, AB53 4PA", "Delivery");

                CreatedOrder(context, sunnySideFarm, 0.19,  StatusConstant.Delivered, DateTime.Now.AddDays(-14), DateTime.Now, null,"Sunnyside House, Maryculter, Aberdeen AB12 5GT", "Delivery");
                CreatedOrder(context, sunnySideFarm, 0.025, StatusConstant.Pending, DateTime.Now.AddDays(-3), null, null, "Sunnyside House, Maryculter, Aberdeen AB12 5GT", "Delivery");
                CreatedOrder(context, sunnySideFarm, 0.025, StatusConstant.Delivered, DateTime.Now.AddDays(-7), DateTime.Now, null, "Sunnyside House, Maryculter, Aberdeen AB12 5GT", "Collection");
                CreatedOrder(context, sunnySideFarm, 0.025, StatusConstant.Pending, DateTime.Now.AddDays(-4), null, null, "Sunnyside House, Maryculter, Aberdeen AB12 5GT", "Delivery");
                CreatedOrder(context, sunnySideFarm, 0.025, StatusConstant.Delivered, DateTime.Now.AddDays(-14), DateTime.Now, null, "Sunnyside House, Maryculter, Aberdeen AB12 5GT", "Collection");
                CreatedOrder(context, sunnySideFarm, 0.15, StatusConstant.Cancelled, DateTime.Now.AddDays(-9), null, DateTime.Now, "Sunnyside House, Maryculter, Aberdeen AB12 5GT", "Delivery");
                CreatedOrder(context, sunnySideFarm, 0.30, StatusConstant.Cancelled, DateTime.Now.AddDays(-8), null, DateTime.Now, "Sunnyside House, Maryculter, Aberdeen AB12 5GT", "Delivery");

                context.SaveChanges();
            }
        }

        private Client CreateClient(DataContext context, string name, string contactName, string telephone, string emailAddress, string billingAddress, string password) {
            hashUtility.GetHashAndSaltString(password, out string passwordHash, out string salt);
            var client = new Client {
                ClientId = new Guid(),
                Name = name,
                ContactName = contactName,
                Telephone = telephone,
                EmailAddress = emailAddress,
                BillingAddress = billingAddress,
                Password = passwordHash,
                Salt = salt
                };
            context.Clients.Add(client);

            return client;
        }

        private Order CreatedOrder(DataContext context, Client client, double quantity, string status, DateTime dateOrdered, DateTime? dateDelivered, DateTime? dateCancelled, string deliveryAddress, string deliveryMethod) {

            var order = new Order {
                OrderId = new Guid(),
                ClientId = client.ClientId,
                Quantity = quantity,
                DeliveryMethod = deliveryMethod,
                Status = status,
                DateOrdered = dateOrdered,
                DateDelivered = dateDelivered,
                DateCancelled = dateCancelled,
                DeliveryAddress = deliveryAddress,
                };

            context.Orders.Add(order);

            return order;
        }

        private User CreateUser(DataContext context, string role, string name, string emailAddress, string password) {
            hashUtility.GetHashAndSaltString(password, out string passwordHash, out string salt);

            var user = new User {
                UserId = Guid.NewGuid(),
                Role = role,
                Name = name,
                EmailAddress = emailAddress,
                Phone = "01224 961370",
                Password = passwordHash,
                Salt = salt,
                Created = DateTime.UtcNow
            };

            context.Users.Add(user);

            logger.LogInformation(string.Format("Created user: {0}", name));

            return user;
        }
    }
}

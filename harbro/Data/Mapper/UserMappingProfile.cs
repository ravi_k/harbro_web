﻿using AutoMapper;
using Harbro.Web.Data.Model;
using Harbro.Web.ViewModel.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Mapper {
    public class UserMappingProfile : Profile {
        public UserMappingProfile() {
            CreateMap<User, UserModel>();

            CreateMap<UserModel, User>()
                .ForMember(m => m.UserId, p => p.Ignore())
                .ForMember(m => m.Created, p => p.Ignore())
                .ForMember(m => m.Accessed, p => p.Ignore());
            }
        }
    }

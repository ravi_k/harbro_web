﻿using AutoMapper;
using Harbro.Web.Data.Model;
using Harbro.Web.ViewModel.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Mapper {
    public class ClientMappingProfile : Profile {
        public ClientMappingProfile() {
            CreateMap<Client, ClientModel>();

            CreateMap<ClientModel, Client>()
                .ForMember(m => m.ClientId, p => p.Ignore())
                .ForMember(m => m.Created, p => p.Ignore())
                .ForMember(m => m.Accessed, p => p.Ignore());
            }
        }
    }

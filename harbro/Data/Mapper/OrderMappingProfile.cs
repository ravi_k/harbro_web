﻿using AutoMapper;
using Harbro.Web.Data.Model;
using Harbro.Web.ViewModel.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Mapper {
    public class OrderMappingProfile : Profile {
        public OrderMappingProfile() {
            CreateMap<Order, OrderModel>();

            CreateMap<OrderModel, Order>()
                .ForMember(m => m.OrderId, p => p.Ignore());
            }
        }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Constants {
    public static class SpeciesConstant {

        public const string Beef = "BEE";
        public const string Sheep = "SHE";
        public const string Dairy = "DAI";
        public const string Pig = "PIG";
        public const string Poultry = "POU";
        public const string Equine = "EQU";
        public const string Pet = "PET";


        public static string GetDescription(string value) {
            return !string.IsNullOrEmpty(value) && ValuesAndDescriptions.ContainsKey(value) ? ValuesAndDescriptions[value] : null;
        }

        public static string GetValue(string description) {
            return ValuesAndDescriptions.Where(v => v.Value == description).Select(v => v.Key).FirstOrDefault();
        }

        public static Dictionary<string, string> ValuesAndDescriptions {
            get {
                return new Dictionary<string, string> {
                    {Beef,"Beef"},
                    {Sheep,"Sheep"},
                    {Dairy,"Dairy"},
                    {Pig,"Pig"},
                    {Poultry,"Poultry"},
                    {Equine,"Equine"},
                     {Pet,"Pet"},
                };
            }
        }
    }
}

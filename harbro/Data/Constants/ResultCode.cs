﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Constants
{
    public enum ResultCode
    {
        Success = 0,
        Error = 1
    }
}

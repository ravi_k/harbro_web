﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Constants {
    public static class StatusConstant {

        public const string Pending = "PEN";
        public const string Delivered = "DEL";
        public const string Cancelled = "CAN";

        public static string GetDescription(string value) {
            return !string.IsNullOrEmpty(value) && ValuesAndDescriptions.ContainsKey(value) ? ValuesAndDescriptions[value] : null;
        }

        public static string GetValue(string description) {
            return ValuesAndDescriptions.Where(v => v.Value == description).Select(v => v.Key).FirstOrDefault();
        }

        public static Dictionary<string, string> ValuesAndDescriptions {
            get {
                return new Dictionary<string, string> {
                    {Pending,"Pending"},
                    {Delivered,"Delivered"},
                    {Cancelled,"Cancelled"},
                };
            }
        }
    }
}

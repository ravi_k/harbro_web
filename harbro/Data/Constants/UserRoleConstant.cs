﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Data.Constants {
    public static class UserRoleConstant {

        public const string Administrator = "ADM";
        public const string User = "USR";

        public static string GetDescription(string value) {
            return !string.IsNullOrEmpty(value) && ValuesAndDescriptions.ContainsKey(value) ? ValuesAndDescriptions[value] : null;
        }

        public static string GetValue(string description) {
            return ValuesAndDescriptions.Where(v => v.Value == description).Select(v => v.Key).FirstOrDefault();
        }

        public static Dictionary<string, string> ValuesAndDescriptions {
            get {
                return new Dictionary<string, string> {
                    {Administrator,"Administrator"},
                    {User,"User"}
                };
            }
        }
    }
}

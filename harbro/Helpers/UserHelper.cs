﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Harbro.Web.Models;

namespace Harbro.Web.Helpers {
    public class UserHelper {
        private static CurrentUserInfo getUserClaims(ClaimsIdentity claimsIdentity) {
            CurrentUserInfo userInfo = new CurrentUserInfo();

            if (claimsIdentity != null && userInfo != null) {

                Claim emailClaim = claimsIdentity.FindFirst(ClaimTypes.Email);
                if (emailClaim != null)
                    userInfo.EmailAddress = emailClaim.Value;
                }
            return userInfo;
            }

        public static CurrentUserInfo GetUserInfo(ControllerBase controllerContext) {
            var claimsIdentity = controllerContext.User.Identity as ClaimsIdentity;
            return getUserClaims(claimsIdentity);
            }
        }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Models {
    public class CurrentUserInfo {
        public string EmailAddress { get; set; }
        }
    }

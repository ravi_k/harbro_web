﻿using FluentEmail.Core;
using FluentEmail.Razor;
using FluentEmail.SendGrid;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Utility {
    public static class ActionUtility {

        public static string IsActive(this IHtmlHelper helper, string control, string action) {
            var routeData = helper.ViewContext.RouteData;

            var routeAction = (string)routeData.Values["action"];
            var routeControl = (string)routeData.Values["controller"];

            var returnActive = control == routeControl && action == routeAction;

            return returnActive ? "active" : "";
        }

        public static bool IsCurrentAction(this IHtmlHelper helper, string action, string controller) {
            var currentAction = helper.ViewContext.RouteData.Values["action"];
            return IsCurrentController(helper, controller) && (string)currentAction == action;
        }

        public static bool IsCurrentController(this IHtmlHelper helper, string controller) {
            var currentController = helper.ViewContext.RouteData.Values["controller"];
            return (string)currentController == controller;
        }
    }
}


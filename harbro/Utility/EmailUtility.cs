﻿using FluentEmail.Core;
using FluentEmail.Razor;
using FluentEmail.SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.Utility {
    public static class EmailUtility {

        public static async Task SendPasswordResetEmail(string emailAddress, string name, string url) {
            Email.DefaultRenderer = new RazorRenderer();
            Email.DefaultSender = new SendGridSender("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");

            var template = "<p style='font-family:Arial,sans-serif;'>Hi @Model.Name,</p><p style='font-family:Arial,sans-serif;'>Please click on the link below to reset your forgotten password:</p><p style='font-family:Arial,sans-serif;'><a href='@Model.Url'>Reset Password</a></p><p style='font-family:Arial,sans-serif;'>Regards,</p><p style='font-family:Arial,sans-serif;'>Harbro Support</p>";

            var email = Email.From("appsupport@harbro.co.uk", "Harbro")
                .To(emailAddress, name)
                .Subject("Reset Forgotten Password")
                .UsingTemplate(template, new { Name = name, Url = url });

            await email.SendAsync();
        }
    }
}

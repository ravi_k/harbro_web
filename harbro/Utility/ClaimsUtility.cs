﻿using Harbro.Web.Data.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Harbro.Web.Utility {
    public static class ClaimsUtility {

        public static bool IsAdmin(this ClaimsPrincipal claimsPrincipal) {
            return claimsPrincipal.IsInRole(UserRoleConstant.Administrator);
        }

        public static bool IsUser(this ClaimsPrincipal claimsPrincipal) {
            return claimsPrincipal.IsInRole(UserRoleConstant.User);
        }

        public static string UserId(this ClaimsPrincipal claimsPrincipal) {
            return claimsPrincipal.GetClaim(ClaimTypes.NameIdentifier);
        }

        public static string Name(this ClaimsPrincipal claimsPrincipal) {
            return claimsPrincipal.Identity.Name;
        }

        public static string EmailAddress(this ClaimsPrincipal claimsPrincipal) {
            return claimsPrincipal.GetClaim(ClaimTypes.Email);
        }
        public static string RoleDescription(this ClaimsPrincipal claimsPrincipal) {
            return UserRoleConstant.GetDescription(claimsPrincipal.Role());
        }
        public static string Role(this ClaimsPrincipal claimsPrincipal) {
            return claimsPrincipal.GetClaim(ClaimTypes.Role);
        }
        private static string GetClaim(this ClaimsPrincipal claimsPrincipal, string type) {
            var claimsIdentity = (ClaimsIdentity)claimsPrincipal.Identity;
            var claim = claimsIdentity.Claims.FirstOrDefault(x => x.Type == type);
            return (claim != null) ? claim.Value : string.Empty;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.Client {
    public class CustomerModel {

        public string AccountNum { get; set; }

        public string Name { get; set; }

        }
    }

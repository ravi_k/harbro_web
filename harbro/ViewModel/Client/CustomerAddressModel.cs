﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.Client {
    public class CustomerAddressModel {

        [Key]
        public string Address { get; set; }

        public string Name { get; set; }

        }
    }

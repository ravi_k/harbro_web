﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.Client {
    public class CustomerOrderModel {

        public string CustAccount { get; set; }

        public string SalesId { get; set; }

        public int SalesType { get; set; }

        public string SalesType_Display { get; set; }

        public string DeliveryAddress { get; set; }

        public string ItemId { get; set; }

        public string ItemName { get; set; }

        public string ItemClassTwoId { get; set; }

        public string ItemClassTwoName { get; set; }

        public DateTime? Date_Ordered { get; set; }

        public DateTime Date_Required { get; set; }

        public decimal Qty_Ordered { get; set; }

        public decimal Qty_Remaining { get; set; }
        }
    }

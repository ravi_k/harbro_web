﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.Client {
    public class ClientModel {

        public Guid ClientId { get; set; }

        public string CustomerNumber { get; set; }

        public string Name { get; set; }

        public string ContactName { get; set; }

        public string EmailAddress { get; set; }

        public string Telephone { get; set; }

        public string BillingAddress { get; set; }


        public List<string> Billing {
            get {
                var strings = new List<string>();
                return this.BillingAddress.Split(',').ToList();
                }
            }

        public string Password { get; set; }

        public string Salt { get; set; }

        public DateTime? Accessed { get; set; }

        public DateTime Created { get; set; }

        }
    }

﻿using Harbro.Web.Data.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.User {    
    public class UserModel {

        public Guid UserId { get; set; }

      
        public string Name { get; set; }
    
        public string EmailAddress { get; set; }

      
        public string Phone { get; set; }

       
        public string Role { get; set; }

        public string RoleDescription {
            get {
                return UserRoleConstant.GetDescription(this.Role);
            }
        }
    }
}

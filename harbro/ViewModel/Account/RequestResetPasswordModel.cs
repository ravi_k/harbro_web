﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.Account
{
    public class RequestResetPasswordModel {

        public string EmailAddress { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.Account
{
    public class AccountModel {

        [Required]
        [DisplayName("Name*")]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [DisplayName("Email*")]
        public string EmailAddress { get; set; }

        [Required]
        [DisplayName("Phone*")]
        public string Phone { get; set; }

        public string ReturnUrl { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.Account
{
    public class ChangePasswordModel {


        [Required]
        [MaxLength(255)]
        [DataType(DataType.Password)]
        [DisplayName("Current Password")]
        public string CurrentPassword { get; set; }

        [Required]
        [MaxLength(255)]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$", ErrorMessage = "Invalid new password, all passwords must be at least 8 characters long and contain one uppercase letter, one lowercase letter and at least one number")]
        [DisplayName("New Password")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Your confirmation password must match your new password")]
        [DisplayName("Confirm New Password")]
        public string ConfirmNewPassword { get; set; }

        public string ReturnUrl { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.Message
{
    public enum MessageType
    {
        Info,
        Error,
        Warning,
        Success
    }
}

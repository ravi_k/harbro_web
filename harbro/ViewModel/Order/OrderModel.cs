﻿using Harbro.Web.Data.Constants;
using Harbro.Web.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Harbro.Web.ViewModel.Order {
    public class OrderModel {
        public Guid OrderId { get; set; }

        public Guid ClientId { get; set; }

        public Harbro.Web.Data.Model.Client Client { get; set; }

        public string ClientNumber {
            get {
                if (Client != null && !string.IsNullOrWhiteSpace(Client.CustomerNumber)) {
                    return Client.CustomerNumber;
                    } else {
                    return string.Empty;
                    }
                }
            }
        public Decimal Quantity { get; set; }

        public Decimal TotalQuantity {
            get {
                if(this.Quantity == (decimal)0.01) {
                    return 1;
                    } else if (this.Quantity == (decimal)0.02) {
                    return 2;
                    } else if (this.Quantity == (decimal)0.03) {
                    return 3;
                    } else if (this.Quantity == (decimal)0.04) {
                    return 4;
                    } else if (this.Quantity == (decimal)0.05) {
                    return 5;
                    } else if (this.Quantity == (decimal)0.06) {
                    return 6;
                    } else if (this.Quantity == (decimal)0.07) {
                    return 7;
                    } else if (this.Quantity == (decimal)0.08) {
                    return 8;
                    } else if (this.Quantity == (decimal)0.09) {
                    return 9;
                    }
                return Quantity;
                }
            }

        public string QuantityString {
            get {
                return this.TotalQuantity.ToString();
                }
            }

        public int Amount { get; set; }

        public string Units { get; set; }

        public string Notes { get; set; }

        public string DeliveryMethod { get; set; }

        public string Status { get; set; }

        public string StatusDescription {
            get {
                return StatusConstant.GetDescription(this.Status);
            }
        }

        public string StatusTextColour {
            get {
                if (this.Status == StatusConstant.Delivered) {
                    return "#008000";
                } else if (this.Status == StatusConstant.Pending) {
                    return "#B03060";
                } else if (this.Status == StatusConstant.Cancelled) {
                    return "#CC0000";
                }
                return "#000";
            }
        }

        public bool IsDelivered {
            get {
                return this.Status == StatusConstant.Delivered;
            }
        }

        public bool IsPending {
            get {
                return this.Status == StatusConstant.Pending;
            }
        }

        public bool IsCancelled {
            get {
                return this.Status == StatusConstant.Cancelled;
            }
        }

        public DateTime DateOrdered { get; set; }

        public string DateString {
            get {
                return this.DateOrdered.ToString("dd-MMM-yyyy");
                }
            }

        public DateTime? DateDelivered { get; set; }

        public DateTime? DateCancelled { get; set; }

        public DateTime? DateRequired { get; set; }

        public string ItemName { get; set; }

        public string ItemId { get; set; }

        public string DeliveryAddress { get; set; }

        public List<string> Delivery {
            get {
                var strings = new List<string>();
                return this.DeliveryAddress.Split(',').ToList();

                }
            }
        }
}
